# GinVoice - Creating LaTeX invoices with a GTK GUI
# Copyright (C) 2021  Erik Nijenhuis <erik@xerdi.com>
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <https://www.gnu.org/licenses/>.

import os
import shutil

from ginvoice.i18n import _
from ginvoice.model.form import FormEvent
from ginvoice.ui.info import InfoWindow
from ginvoice.ui.variable import VariableEntry
from ginvoice.environment import image_dir, customer_info_file, supplier_info_file
from ginvoice.model.column import TableColumnStore, CumulativeColumnStore, Column, TableColumnHandler, \
    CumulativeColumnHandler
from ginvoice.model.info import GenericInfoStore
from ginvoice.model.style import Style
from ginvoice.util import find_ui_file
from ginvoice.model.preference import preference_store

from ginvoice.gtk import Gtk, Gdk, Gio


def parse_bool(v):
    if isinstance(v, str):
        return v == 'True'
    return v


@Gtk.Template.from_file(find_ui_file("preferences.glade"))
class PreferencesWindow(Gtk.Window):

    __gtype_name__ = "preferences_window"

    title = Gtk.Template.Child()
    subtitle = Gtk.Template.Child()
    author = Gtk.Template.Child()
    keywords = Gtk.Template.Child()

    main_font = Gtk.Template.Child()
    mono_font = Gtk.Template.Child()
    fg_color = Gtk.Template.Child('foreground_color')
    bg_color = Gtk.Template.Child('background_color')

    invoice_ending = Gtk.Template.Child()

    footer_image_1 = Gtk.Template.Child()
    footer_image_2 = Gtk.Template.Child()
    footer_image_3 = Gtk.Template.Child()

    invoice_counter = Gtk.Template.Child()
    customer_counter = Gtk.Template.Child()

    pdf_viewer = Gtk.Template.Child()

    customer_removal = Gtk.Template.Child()
    invoice_removal = Gtk.Template.Child()
    record_removal = Gtk.Template.Child()

    locale = Gtk.Template.Child()
    babel = Gtk.Template.Child()
    currency = Gtk.Template.Child()

    customer_info_table = Gtk.Template.Child()
    supplier_info_table = Gtk.Template.Child()

    table_column_group = Gtk.Template.Child()
    cumulative_column_group = Gtk.Template.Child()

    customer_info_store = GenericInfoStore(customer_info_file)
    supplier_info_store = GenericInfoStore(supplier_info_file)

    settings_stack = Gtk.Template.Child()

    table_columns = TableColumnStore()
    cumulative_columns = CumulativeColumnStore()

    def __init__(self, form_registry: FormEvent, section=None):
        Gtk.Window.__init__(self)
        self.table_columns.load()
        self.title.set_text(preference_store['title'].value)
        self.subtitle.set_text(preference_store['subtitle'].value)
        self.author.set_text(preference_store['author'].value)
        self.keywords.set_text(preference_store['keywords'].value)
        self.event = form_registry

        if section:
            self.settings_stack.set_visible_child_name(section)

        def filter_mono(family, face, include):
            return family.is_monospace() == include

        self.main_font.set_filter_func(filter_mono, False)
        if preference_store['main_font'].value:
            self.main_font.set_font(preference_store['main_font'].value)
        self.mono_font.set_filter_func(filter_mono, True)
        if preference_store['mono_font'].value:
            self.mono_font.set_font(preference_store['mono_font'].value)
        fg_color = Gdk.RGBA()
        fg_color.parse(preference_store['foreground_color'].value)
        self.fg_color.set_rgba(fg_color)
        bg_color = Gdk.RGBA()
        bg_color.parse(preference_store['background_color'].value)
        self.bg_color.set_rgba(bg_color)

        self.invoice_ending.set_text(preference_store['invoice_ending'].value)

        self.footer_image_1.add_shortcut_folder(image_dir)
        self.footer_image_2.add_shortcut_folder(image_dir)
        self.footer_image_3.add_shortcut_folder(image_dir)
        if preference_store['footer_image_1'].value:
            self.footer_image_1.set_filename(os.path.join(image_dir, preference_store['footer_image_1'].value))
        if preference_store['footer_image_2'].value:
            self.footer_image_2.set_filename(os.path.join(image_dir, preference_store['footer_image_2'].value))
        if preference_store['footer_image_3'].value:
            self.footer_image_3.set_filename(os.path.join(image_dir, preference_store['footer_image_3'].value))

        self.invoice_counter.set_text(str(preference_store['invoice_counter'].value))
        self.customer_counter.set_text(str(preference_store['customer_counter'].value))

        if preference_store['pdf_viewer'].value:
            current_viewer = preference_store['pdf_viewer'].value
            self.pdf_viewer.append_custom_item(current_viewer, _('Default ') + current_viewer, Gio.ThemedIcon(name="application-pdf"))
            self.pdf_viewer.set_active_custom_item(current_viewer)

        self.customer_removal.set_active(parse_bool(preference_store['show_customer_removal'].value))
        self.invoice_removal.set_active(parse_bool(preference_store['show_invoice_removal'].value))
        self.record_removal.set_active(parse_bool(preference_store['show_record_removal'].value))

        self.locale.get_model()[0][1] = ''
        self.locale.set_active_id(preference_store['locale'].value)
        self.currency.set_active_id(preference_store['currency'].value)
        self.babel.set_active_id(preference_store['babel'].value)

        self.customer_info_store.load()
        self.supplier_info_store.load()
        self.customer_info_table.set_model(self.customer_info_store)
        self.supplier_info_table.set_model(self.supplier_info_store)

        table_column_rows = self.table_column_group.get_children()
        self.table_columns.load()
        # Setup default values from glade defaults
        if len(table_column_rows) != len(self.table_columns):
            for row in table_column_rows:
                c = Column()
                l, t, s, t = row.get_children()
                c.title = l.get_text()
                c.size_type = s.get_model()[s.get_active_iter()][0]
                c.text = t.get_text()
                self.table_columns.append(c)
        for idx, row in enumerate(table_column_rows):
            TableColumnHandler(*row.get_children()[1:], self.table_columns[idx])

        cumulative_column_rows = self.cumulative_column_group.get_children()
        self.cumulative_columns.load()
        if len(cumulative_column_rows) != len(self.cumulative_columns):
            for row in cumulative_column_rows:
                c = Column()
                l, t, s = row.get_children()
                c.title = t.get_text()
                c.size_type = int(s.get_active())
                self.cumulative_columns.append(c)
        for idx, row in enumerate(cumulative_column_rows):
            CumulativeColumnHandler(*row.get_children()[1:], self.cumulative_columns[idx])

    @Gtk.Template.Callback()
    def validate_number(self, widget):
        raw = widget.get_text()
        val = ''.join([c for c in raw if c.isdigit()])
        widget.set_text(val)
        return raw == val

    @Gtk.Template.Callback()
    def change_title(self, widget):
        preference_store['title'] = widget.get_text()

    @Gtk.Template.Callback()
    def change_subtitle(self, widget):
        preference_store['subtitle'] = widget.get_text()

    @Gtk.Template.Callback()
    def change_author(self, widget):
        preference_store['author'] = widget.get_text()

    @Gtk.Template.Callback()
    def change_keywords(self, widget):
        preference_store['keywords'] = widget.get_text()

    @Gtk.Template.Callback()
    def change_main_font(self, widget):
        preference_store['main_font'] = widget.get_font_family().get_name()

    @Gtk.Template.Callback()
    def change_mono_font(self, widget):
        preference_store['mono_font'] = widget.get_font_family().get_name()

    @staticmethod
    def _rgba_to_hex(rgba):
        return "#" + "".join(["%02x" % int(c * 255) for c in [rgba.red, rgba.green, rgba.blue]])

    @Gtk.Template.Callback()
    def change_fg_color(self, widget):
        preference_store['foreground_color'] = self._rgba_to_hex(widget.get_rgba())

    @Gtk.Template.Callback()
    def change_bg_color(self, widget):
        preference_store['background_color'] = self._rgba_to_hex(widget.get_rgba())

    @Gtk.Template.Callback()
    def change_footer_img1(self, widget):
        preference_store['footer_image_1'] = widget.get_filename()

    @Gtk.Template.Callback()
    def change_footer_img2(self, widget):
        preference_store['footer_image_2'] = widget.get_filename()

    @Gtk.Template.Callback()
    def change_footer_img3(self, widget):
        preference_store['footer_image_3'] = widget.get_filename()

    @Gtk.Template.Callback()
    def change_invoice_counter(self, widget):
        preference_store['invoice_counter'] = widget.get_text()

    @Gtk.Template.Callback()
    def change_customer_counter(self, widget):
        preference_store['customer_counter'] = widget.get_text()

    @Gtk.Template.Callback()
    def change_locale(self, combobox):
        preference_store['locale'] = combobox.get_active_id()

    @Gtk.Template.Callback()
    def change_babel(self, combobox):
        preference_store['babel'] = combobox.get_active_id()

    @Gtk.Template.Callback()
    def change_currency(self, combobox):
        preference_store['currency'] = combobox.get_active_id()

    @Gtk.Template.Callback()
    def change_confirmation(self, switch, state):
        preference_store[switch.get_name()] = state

    @Gtk.Template.Callback()
    def changed_pdf_viewer(self, btn):
        if hasattr(btn.get_app_info(), 'get_executable'):
            preference_store['pdf_viewer'] = btn.get_app_info().get_executable()

    @Gtk.Template.Callback()
    def open_customer_info(self, btn):
        dialog = InfoWindow(_('Add customer record'), self.customer_info_store)
        dialog.set_transient_for(self)
        dialog.show_all()

    @Gtk.Template.Callback()
    def open_supplier_info(self, btn):
        dialog = InfoWindow(_('Add supplier record'), self.supplier_info_store)
        dialog.set_transient_for(self)
        dialog.show_all()

    @Gtk.Template.Callback()
    def edit_customer_info(self, treeview):
        dialog = InfoWindow(_('Edit customer record'), self.customer_info_store,
                            treeview.get_selection().get_selected()[1])
        dialog.set_transient_for(self)
        dialog.show_all()

    @Gtk.Template.Callback()
    def edit_supplier_info(self, treeview):
        dialog = InfoWindow(_('Edit supplier record'), self.supplier_info_store,
                            treeview.get_selection().get_selected()[1])
        dialog.set_transient_for(self)
        dialog.show_all()

    @Gtk.Template.Callback()
    def remove_info(self, treeview):
        model, iter = treeview.get_selection().get_selected()
        model.remove(iter)

    @Gtk.Template.Callback()
    def change_invoice_ending(self, entry):
        preference_store['invoice_ending'] = entry.get_text()

    @Gtk.Template.Callback()
    def info_row_activated(self, btn, *args):
        btn.emit('clicked')

    @Gtk.Template.Callback()
    def save_changes(self, btn):
        for k in ['footer_image_1', 'footer_image_2', 'footer_image_3']:
            val = preference_store[k].value
            if val and os.path.exists(val):
                shutil.copy(val, image_dir)
                preference_store[k] = os.path.basename(val)
        preference_store.commit()
        self.customer_info_store.commit()
        self.supplier_info_store.commit()
        self.table_columns.commit()
        self.cumulative_columns.commit()
        self.event.emit('saved')
        self.destroy()

    @Gtk.Template.Callback()
    def cancel_changes(self, btn):
        preference_store.load()
        self.table_columns.load()
        self.cumulative_columns.load()
        self.customer_info_store.load()
        self.supplier_info_store.load()
        self.event.emit('canceled')
        self.destroy()


if __name__ == '__main__':
    Style()
    window = PreferencesWindow(FormEvent(), section='info')
    window.connect("destroy", Gtk.main_quit)
    window.show_all()
    Gtk.main()
