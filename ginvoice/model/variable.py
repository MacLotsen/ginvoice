# GinVoice - Creating LaTeX invoices with a GTK GUI
# Copyright (C) 2021  Erik Nijenhuis <erik@xerdi.com>
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <https://www.gnu.org/licenses/>.

from ginvoice.gtk import GObject
from ginvoice.i18n import _


class Variable(GObject.GObject):
    key = GObject.Property(type=str)
    value = GObject.Property(type=str)

    def __init__(self):
        GObject.GObject.__init__(self)

    def __repr__(self):
        if self.value:
            return self.value
        return _(self.key)
