
## Changelog

To insert the date, use the following: `date -R` (RFC 5322 style).

For more info, visit [this link](https://man7.org/linux/man-pages/man5/deb-changelog.5.html) or type `man deb-changelog`


<!--
    Further helpful sources
    https://trstringer.com/creating-python-pkg-ubuntu/
  -->
